# PRACE CodeVault

PRACE CodeVault is an open repository containing various high
performance computing code samples. The project aims to support
self-learning of HPC programming and will be used as an Open platform
for the HPC community to share example code snippets, proof-of-concept
codes and so forth.  

CodeVault contains training material from PRACE partners, as well as
example codes of common HPC kernels such as dense and sparse linear
algebra, spectral and N-body methods, structured and unstructured
grids, Monte Carlo methods and parallel I/O. The code samples are
published as open source and can be used both for educational purposes
and as parts of real application suites (as permitted by particular
license).  

## How to contribute

Any contributions (new code samples, bug fixes, improvements etc.) are
warmly welcome. In order to contribute, please follow the standard
Gitlab workflow:

1. Fork the project into your personal space on GitLab.com
2. Create a feature branch
3. Work on your contributions
4. Push the commit(s) to your fork
5. Submit a merge request to the master branch

## Instructions for cloning/building a single example

### Cloning a single example

Since version 1.7 git supports sparse checkouts, allowing users only to clone specific directories and not the whole repository structure.
In order to do this we provide an example for cloning the `n-body_methods/dynamic_sparse_data_exchange` example. The steps are:

```bash
# Create empty repository with name 'CodeVault' 
# (you may use any other name)
git init CodeVault
cd CodeVault

# Set upstream repository
git remote add origin https://gitlab.com/PRACE-4IP/CodeVault.git

# Enable sparse checkout
git config core.sparsecheckout true

# List directories for sparse checkout
#  * The 'cmake' directory is necessary for the build system
#  * Adapt the second entry according to the example(s) of your interest
cat >.git/info/sparse-checkout <<END
/hpc_kernel_samples/cmake
/hpc_kernel_samples/n-body_methods/dynamic_sparse_data_exchange
END

# Do sparse checkout
git pull
git checkout master
```

### Building a single example

In order to build the example we cloned above, follow the typical CMake steps:

```bash
cd hpc_kernel_samples/n-body_methods/dynamic_sparse_data_exchange
mkdir build
cd build
cmake ..
make
make install # optional
```

This will compile the example provided that MPI is found.

