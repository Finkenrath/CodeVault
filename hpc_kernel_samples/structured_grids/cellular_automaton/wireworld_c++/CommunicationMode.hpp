#pragma once

#include <array>
#include <tuple>

enum class CommunicationMode {
	Collective, //
	P2P         //
};

constexpr std::array<std::pair<const char*, CommunicationMode>, 2> StringToCommunicationMode() {
	return {{
	    std::make_pair("Collective", CommunicationMode::Collective), //
	    std::make_pair("P2P", CommunicationMode::P2P)                //
	}};
}
