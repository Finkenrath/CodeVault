#pragma once

#include <boost/container/static_vector.hpp>
#include <mpi.h>

class MpiRequest {
	constexpr static std::size_t NoNeighbors{ 8 };

  public:
	template <typename T> using DoubleVector = boost::container::static_vector<T, NoNeighbors * 2>;

  private:
	DoubleVector<MPI_Request> reqs_;
	bool finished{};

  public:
	MpiRequest(DoubleVector<MPI_Request> reqs) : reqs_(reqs) {}

	MpiRequest(const MpiRequest&) = default;
	MpiRequest(MpiRequest&&) = default;
	MpiRequest& operator=(const MpiRequest&) = default;
	MpiRequest& operator=(MpiRequest&&) = default;
	void Wait() {
		MPI_Waitall(static_cast<int>(reqs_.size()), //
		            reqs_.data(),                   //
		            MPI_STATUSES_IGNORE);           //
		finished = true;
	}
	~MpiRequest() {
		if (!finished) { MpiReportErrorAbort("Forgot to Wait for MPI_Request"); }
	}
};
