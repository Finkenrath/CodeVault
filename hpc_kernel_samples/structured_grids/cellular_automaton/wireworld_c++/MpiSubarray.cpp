#include "MpiSubarray.hpp"

SubarrayDefinition::SubarrayDefinition(
    std::initializer_list<SubarrayDimensionDefinition> saDimDefs) {
	for (const auto& dd : saDimDefs) {
		sizes_.push_back(static_cast<int>(dd.size));
		subSizes_.push_back(static_cast<int>(dd.subSize));
		starts_.push_back(static_cast<int>(dd.start));
	}
}

void MpiSubarray::swap(MpiSubarray& first, MpiSubarray& second) noexcept {
	using std::swap;
	swap(first.type_, second.type_);
}

MpiSubarray::MpiSubarray(SubarrayDefinition sd) {
	MPI_Type_create_subarray(static_cast<int>(sd.dims()), // ndims
	                         sd.sizes(),                  // array_of_sizes
	                         sd.subSizes(),               // array_of_subsizes
	                         sd.starts(),                 // array_of_starts
	                         MPI_ORDER_C,                 // order
	                         MPI_CHAR,                    // oldtype
	                         &type_                       // newtype
	                         );
	MPI_Type_commit(&type_);
}
MpiSubarray::~MpiSubarray() {
	if (type_ != MPI_DATATYPE_NULL) { MPI_Type_free(&type_); }
}
MpiSubarray::MpiSubarray(MpiSubarray&& other) noexcept { swap(*this, other); }
MpiSubarray& MpiSubarray::operator=(MpiSubarray&& other) noexcept {
	swap(*this, other);
	return *this;
}
