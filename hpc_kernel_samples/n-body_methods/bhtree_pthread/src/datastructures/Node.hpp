#ifndef TREE_NODE_HPP
#define TREE_NODE_HPP

#include "Body.hpp"
#include "Box.hpp"
#include "Tree.hpp"
#include "BarnesHutTree.hpp"
#include "ReadWriteLock.hpp"
#include <cstdlib>
#include <vector>

namespace nbody {
	using namespace std;

	//class for storing node information
	class Node {
		friend class Tree;
		friend class BarnesHutTree;
		friend class BarnesHutTreeThreaded;
		friend class PthreadSimulation;
	protected:
		Box bb;
		vector<Body> bodies;
		Node* prev;
		Node* next;
		Node* nextSibling;
		Node* prevSibling;
		Node* parent;
		Node* afterSubtree;
		bool leaf;
		Tree* tree;
		Body representative;
		ReadWriteLock lock;
	public:
		Node(Tree* tree);
		virtual ~Node();
		virtual bool isSplitable();
		virtual void extendBBforBodies();
		virtual void extendBBtoCube();
		virtual Box getBB();
		virtual void setBB(Box bb);
		virtual vector<Body> getBodies();
		virtual void insertBefore(Node* node);
		virtual void insertAfter(Node* node);
		virtual void unlink();
		virtual void update();
		virtual double getL();
		virtual bool isCorrect();
		virtual void print(int parallelId);
		virtual bool sufficientForBody(Body body);
		virtual bool sufficientForBox(Box box);
		virtual void setBodies(vector<Body> bodies);
		virtual void extractLocalBodiesTo(vector<Body>& bodies);
	};
}

#endif
