#ifndef TREE_HPP
#define TREE_HPP

#include <vector>
#include <cstdlib>
#include <string>
#include <pthread.h>
#include <stack>
#include "Body.hpp"
#include "Box.hpp"

namespace nbody {
	using namespace std;

	class Node;
	class Simulation;

	//superclass for Barnes-Hut tree
	class Tree {
		friend class Node;
		friend class PthreadSimulation;
	protected:
		Node* nodes;
		unsigned int maxLeafBodies;
		int parallelId;
		Simulation* simulation;
	public:
		Tree(int parallelId);
		virtual ~Tree();
		virtual void setSimulation(Simulation* simulation);
		virtual void clean();
		virtual void build(vector<Body> bodies) = 0;
		virtual void build(vector<Body> bodies, Box domain) = 0;
		virtual int numberOfChildren() = 0;
		virtual size_t numberOfNodes();
		virtual bool isCorrect();
		virtual void accumulateForceOnto(Body& body);
		virtual void computeForces();
		virtual vector<Body> extractLocalBodies();
		virtual vector<Body> copyRefinements(Box domain);
		virtual void rebuild(Box domain);
		virtual void rebuild(Box domain, vector<Body> bodies);
		virtual void rebuild();
		virtual Box getRootBB();
		virtual void print(int parallelId);
		virtual Box advance();
		virtual Box getLocalBB();
	};


}

#endif
